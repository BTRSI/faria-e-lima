import React from 'react';
import { AuthProvider } from './useAuth';
import { LoadingProvider } from './useLoading';
import { SidebarProvider } from './useSidebar';
import { SelectProvider } from './useSelect';

const AppProvider: React.FC = React.memo(({ children }) => (
  <AuthProvider>
    <LoadingProvider>
      <SelectProvider>
        <SidebarProvider>
          {children}
        </SidebarProvider>
      </SelectProvider>
    </LoadingProvider>
  </AuthProvider>
));

export default AppProvider;
