import { Sidebar } from '@/components/Sidebar';
import {
  createContext, ReactNode, useContext, useState,
} from 'react';

interface sidebarContextData {
    open: boolean;
    setOpen: (open: boolean) => void;
}

interface SidebarProviderProps {
    children: ReactNode;
}

const sidebarContext = createContext<sidebarContextData>({} as sidebarContextData);

export function SidebarProvider({ children }: SidebarProviderProps): JSX.Element {
  const [open, setOpen] = useState(false);

  return (
    <sidebarContext.Provider
      value={{ open, setOpen }}
    >
      {children}
      {open && <Sidebar />}
    </sidebarContext.Provider>
  );
}

export function useSidebar(): sidebarContextData {
  const context = useContext(sidebarContext);

  return context;
}
