import {
  createContext, ReactNode, useContext, useState,
} from 'react';

interface SelectContextData {
    isSelect: 'imovel' | 'usuario' | 'cliente';
    setIsSelect: (isSelect: 'imovel' | 'usuario' | 'cliente') => void;
}

interface SelectProviderProps {
    children: ReactNode;
}

const SelectContext = createContext<SelectContextData>({} as SelectContextData);

export function SelectProvider({ children }: SelectProviderProps): JSX.Element {
  const [isSelect, setIsSelect] = useState<'imovel' | 'usuario' | 'cliente'>('cliente');

  return (
    <SelectContext.Provider
      value={{ isSelect, setIsSelect }}
    >
      {children}
    </SelectContext.Provider>
  );
}

export function useSelect(): SelectContextData {
  const context = useContext(SelectContext);

  return context;
}
