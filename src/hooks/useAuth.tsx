import React, {
  createContext, useCallback, useState, useContext,
} from 'react';
import Cookie from 'js-cookie';
import { addDays } from 'date-fns';
import { toast } from 'react-toastify';
import handleErrors from '@/utils/handleErrors';
import { useRouter } from 'next/router';
import { api } from '../services/axios';

interface IUser {
    id_usuario: string;
    nome: string;
    login: string;
    hierarquia: string;
}

interface SignInCredentials {
    l: string;
    p: string;
}

interface AuthContextState {
    usuario: IUser;
    signIn(credentials: SignInCredentials): Promise<void>;
    signOut(): void;
}

interface AuthState {
    token: string;
    usuario: IUser;
}

const AuthContext = createContext<AuthContextState>({} as AuthContextState);

export const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>(() => {
    const token = Cookie.get('token');
    const usuario = Cookie.getJSON('usuario');

    if (token && usuario) {
      console.log('usuario', usuario);

      api.defaults.headers.authorization = token;
      return { token, usuario };
    }

    return {} as AuthState;
  });

  const router = useRouter();

  const signIn = useCallback(async ({ l, p }: SignInCredentials) => {
    try {
      const response = await api.post('/login', {
        usuario: {
          l,
          p,
        },
      });

      const { token, usuario } = response.data;

      Cookie.set('token', token, { expires: addDays(new Date(), 1) });
      Cookie.set('usuario', JSON.stringify(usuario), { expires: addDays(new Date(), 1) });
      api.defaults.headers.authorization = token;

      toast.success('Sucesso ao relizar login');

      setData({ token, usuario });
    } catch (err) {
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else {
        toast.error('Erro ao realizar Login');
      }
    }
  }, []);

  const signOut = useCallback(() => {
    Cookie.remove('token');
    Cookie.remove('usuario');

    router.push('/admin/login');

    delete api.defaults.headers.authorization;

    setData({} as AuthState);
  }, [router]);

  return (
    <AuthContext.Provider value={{ usuario: data.usuario, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth(): AuthContextState {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used whithin an AuthProvider');
  }

  return context;
}
