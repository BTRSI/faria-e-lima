import { FiMapPin, FiCamera } from 'react-icons/fi';
import { useRouter } from 'next/router';
import { useCallback } from 'react';
import styles from './styles.module.scss';

interface IImmobile {
    bairro: string;
    numero: string;
    imagens: string[];
    titulo: string;
    valor: string;
    valorFormatado: string;
    descricao: string;
    id_imovel: string;
    link_maps: string;
    banner: string;
}

interface PropertyProps {
    immobile: IImmobile;
}

export function Property({ immobile }: PropertyProps): JSX.Element {
  const router = useRouter();

  const handleGoToDescription = useCallback((id: string) => {
    router.push(`/descricao/${id}`);
  }, [router]);

  return (
    <div className={styles.propertyContainer}>
      <div className={styles.imageContainer}>
        <img src={`http://mobi.tieng.com.br/imagens/${immobile.id_imovel}/${immobile.banner}`} alt="Imagem do imóvel" />
        <div>
          <a href={immobile.link_maps} target="_blank" rel="noreferrer">
            {' '}
            <FiMapPin />
            {immobile.bairro}
            ,
            {' '}
            {immobile.numero}
          </a>
          <span>
            {' '}
            <FiCamera />
            {immobile.imagens.length}
          </span>
        </div>
      </div>
      <div className={styles.informationContainer}>
        <h1>{immobile.titulo}</h1>
        <strong>{immobile.valorFormatado}</strong>
        <p className={styles.descricao}>{immobile.descricao}</p>
        <div className={styles.buttonContainer}>
          <button type="button" onClick={() => { handleGoToDescription(immobile.id_imovel); }}>
            Ver detalhes
          </button>
        </div>
      </div>
    </div>
  );
}
