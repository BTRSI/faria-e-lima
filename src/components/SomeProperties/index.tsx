import React from 'react';
import { Property } from '../Property';
import styles from './styles.module.scss';

interface SomePropertiesProps {
    immobile: any[];
  }

export function SomeProperties({ immobile }: SomePropertiesProps): JSX.Element {

  return (
    <main className={styles.container}>
      <div className={styles.titleContainer}>
        <h1>Imóveis</h1>
        <p>Estas são as propriedades mais recentes cadastradas.</p>
      </div>
      <div className={styles.slider}>
          {immobile.map((immobileMapped) => (
            <Property
              key={immobileMapped.id_imovel}
              immobile={immobileMapped}
            />
          ))}
      </div>
    </main>
  );
}
