import SidebarComponent from 'react-sidebar';
import { HeaderLandingPage } from '@/components/Header/HeaderLandingPage';
import { useRouter } from 'next/router';
import { HeaderAdminPage } from '../Header/HeaderAdminPage';

export function Sidebar(): JSX.Element {
  const { asPath } = useRouter();
  const checkIfIsAdminRoute = new RegExp('/admin', 'g');

  return (
    <SidebarComponent
      sidebar={(
        <>
          {
                checkIfIsAdminRoute.test(asPath)
                  ? <HeaderAdminPage />
                  : <HeaderLandingPage />
                }
        </>
          )}
      open
      onSetOpen={() => true}
      styles={{ ...stylesDefault }}
    />
  );
}

const stylesDefault = {
  root: {
    position: 'absolute',
    top: '48px',
    left: '0',
    right: '0',
    bottom: '0',
    overflow: 'hidden',
  },
  sidebar: {
    zIndex: '2',
    position: 'absolute',
    top: '0',
    bottom: '0',
    right: '0',
    width: '200px',
    background: '#222222',
    transition: 'transform .3s ease-out',
    WebkitTransition: '-webkit-transform .3s ease-out',
    willChange: 'transform',
    overflowY: 'auto',
  },
  content: {
    position: 'absolute',
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    overflowY: 'auto',
    WebkitOverflowScrolling: 'touch',
    transition: 'left .3s ease-out, right .3s ease-out',
  },
  overlay: {
    zIndex: '1',
    position: 'fixed',
    top: '48px',
    left: '0',
    right: '0',
    bottom: '0',
    opacity: '0',
    visibility: 'hidden',
    transition: 'opacity .3s ease-out, visibility .3s ease-out',
    backgroundColor: 'rgba(0,0,0,0.9)',
  },
  dragHandle: {
    zIndex: '1',
    position: 'fixed',
    top: '0',
    bottom: '0',
  },
};
