import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import handleErrors from '@/utils/handleErrors';
import { useCallback, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { useAuth } from '@/hooks/useAuth';
import { CreateCostumer } from './CreateCostumer';
import { ListCostumer } from './ListCostumer';
import { UpdateCostumer } from './UpdateCostumer';

import styles from './styles.module.scss';

interface ICostumer {
    id_proprietario: string;
    nome: string;
    obs: string;
    contato: string;
}

interface IFilter {
    nome?: string;
    obs?: string;
    contato?: string;
}

export function Costumer(): JSX.Element {
  const { setLoading } = useLoading();
  const { signOut } = useAuth();
  const [costumerTypeSelected, setCostumerTypeSelected] = useState<'listar' | 'criar'>('listar');
  const [costumerSelected, setCostumerSelected] = useState<ICostumer>({} as ICostumer);
  const [filter, setFilter] = useState<IFilter>({} as IFilter);

  const [costumers, setCostumer] = useState<ICostumer[]>([]);

  useEffect(() => {
    setLoading(true);
    api.post('/proprietario/pesquisa', {
      proprietario: {
        ...filter,
      },
    })
      .then((response) => {
        setLoading(false);
        setCostumer(response.data.result);
      })
      .catch((err) => {
        console.error(err.response);
        setLoading(false);
        setCostumer([]);

        if (err.response) {
          if (err.response.status === 401) {
            signOut();
          } else {
            const { message } = handleErrors(err.response);
            toast.error(message[0]);
          }
        } else {
          toast.error('Erro ao buscar clientes');
        }
      });
  }, [setLoading, filter, signOut]);

  const handleOptionSelected = useCallback((option: 'listar' | 'criar') => {
    setCostumerTypeSelected(option);
  }, []);

  const handleUpdateCostumer = useCallback((costumer: ICostumer[]) => {
    setCostumer(costumer);
  }, []);

  const handleSelectACostumer = useCallback((costumer: ICostumer) => {
    setCostumerSelected(costumer);
  }, []);

  const handleFilter = useCallback((filterObject: IFilter) => {
    setFilter(filterObject);
  }, []);

  return (
    <>
      <div className={styles.containerSelect}>
        <nav>
          <button type="button" className={costumerTypeSelected === 'listar' ? `${styles.active}` : ''} onClick={() => { handleOptionSelected('listar'); }}>Todos</button>
        </nav>
        <nav>
          <button type="button" className={costumerTypeSelected === 'criar' ? `${styles.active}` : ''} onClick={() => { handleOptionSelected('criar'); }}>Cadastrar</button>
        </nav>
      </div>
      <div className={styles.content}>
        {(costumerTypeSelected === 'listar' && !costumerSelected.id_proprietario)
            && (
            <ListCostumer
              onHandleSelectACostumer={handleSelectACostumer}
              costumers={costumers}
              filter={filter}
              onHandleFilter={handleFilter}
            />
            )}

        {(costumerTypeSelected === 'listar' && costumerSelected.id_proprietario)
            && (
            <UpdateCostumer
              onHandleSelectACostumer={handleSelectACostumer}
              costumerSelected={costumerSelected}
              onHandleUpdateCostumer={handleUpdateCostumer}
              costumers={costumers}
            />
            )}

        {costumerTypeSelected === 'criar'
                && (
                <CreateCostumer
                  onHandleOptionSelected={handleOptionSelected}
                  onHandleUpdateCostumer={handleUpdateCostumer}
                  costumers={costumers}
                />
                )}
      </div>
    </>
  );
}
