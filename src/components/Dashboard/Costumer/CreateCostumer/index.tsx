import { FormEvent, useCallback, useState } from 'react';
import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import { toast } from 'react-toastify';
import InputMask from 'react-input-mask';

import { formatToCellphone } from '@/utils/formatToCellphone';
import styles from './styles.module.scss';

interface CreateCostumerProps {
    onHandleOptionSelected: (option: 'listar' | 'criar') => void;
    onHandleUpdateCostumer: (user: any) => void;
    costumers: any[];
}

export function CreateCostumer({
  onHandleOptionSelected,
  onHandleUpdateCostumer,
  costumers,
}: CreateCostumerProps): JSX.Element {
  const { setLoading } = useLoading();
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [observation, setObservation] = useState('');

  const handleCreateCostumer = useCallback(async (event: FormEvent) => {
    event.preventDefault();
    setLoading(true);
    try {
      const { data } = await api.post('/proprietario', {
        proprietario: {
          nome: name,
          contato: phone,
          obs: observation,
        },
      });

      onHandleOptionSelected('listar');

      onHandleUpdateCostumer([...costumers, data.proprietario]);

      toast.success('Cliente cadastrado com sucesso!');

      setLoading(false);
    } catch (err) {
      console.log('err', err.message);
      setLoading(false);
      if (err.message) {
        toast.error(err.message);
      }
    }
  }, [
    name, phone, observation, setLoading, costumers,
    onHandleOptionSelected, onHandleUpdateCostumer,
  ]);

  return (
    <form className={styles.form} onSubmit={handleCreateCostumer}>
      <label htmlFor="nome">Nome</label>
      <input
        type="nome"
        id="nome"
        placeholder="Digite o nome"
        value={name}
        onChange={(e) => { setName(e.target.value); }}
      />

      <label htmlFor="telefone">Contato</label>
      <InputMask
        type="text"
        id="phone"
        mask="(99) 99999-9999"
        placeholder="Digite o número de contato"
        value={formatToCellphone(phone)}
        onChange={(e) => { setPhone(e.target.value); }}
      />

      <label htmlFor="obs">Observação</label>
      <input
        id="obs"
        placeholder="Digite sua obs"
        value={observation}
        onChange={(e) => { setObservation(e.target.value); }}
      />

      <button type="submit">Cadastrar</button>
    </form>
  );
}
