import React, { FormEvent, useCallback, useState } from 'react';
import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import { toast } from 'react-toastify';
import InputMask from 'react-input-mask';

import { FiArrowLeft } from 'react-icons/fi';
import handleErrors from '@/utils/handleErrors';
import styles from './styles.module.scss';

interface ICostumer {
    id_proprietario: string;
    nome: string;
    obs: string;
    contato: string;
}

interface UpdateCostumerProps {
    onHandleSelectACostumer: (costumer: ICostumer) => void;
    costumerSelected: ICostumer;
    costumers: ICostumer[];
    onHandleUpdateCostumer: (costumer: ICostumer[]) => void;
}

export function UpdateCostumer({
  costumerSelected,
  onHandleSelectACostumer,
  costumers,
  onHandleUpdateCostumer,
}: UpdateCostumerProps): JSX.Element {
  const { setLoading } = useLoading();

  const [name, setName] = useState(costumerSelected.nome);
  const [phone, setPhone] = useState(costumerSelected.contato);
  const [obs, setObs] = useState(costumerSelected.obs);

  const checkChanges = useCallback((costumer: Omit<ICostumer, 'id_proprietario'>): Promise<Omit<ICostumer, 'id_proprietario'>> => new Promise(
    (resolve) => {
      if (costumer.contato === costumerSelected.contato) {
        delete costumer.contato;
      }

      if (costumer.nome === costumerSelected.nome) {
        delete costumer.nome;
      }

      if (costumer.obs === costumerSelected.obs) {
        delete costumer.obs;
      }
      resolve(costumer);
    },
  ), [costumerSelected]);

  const handleCloseUpdate = useCallback(() => {
    onHandleSelectACostumer({} as any);
  }, [onHandleSelectACostumer]);

  const handleUpdateUser = useCallback(async (event: FormEvent) => {
    event.preventDefault();
    setLoading(true);
    try {
      const costumer = {
        nome: name,
        contato: phone,
        obs,
      };

      const checkedCostumer = await checkChanges(costumer);

      const { data } = await api.put(`/proprietario/${costumerSelected.id_proprietario}`, {
        proprietario: checkedCostumer,
      });

      const findIndexCostumerUpdated = costumers.findIndex((userMapped) => (
        userMapped.id_proprietario === data.proprietario.id_proprietario
      ));

      if (findIndexCostumerUpdated >= 0) {
        costumers[findIndexCostumerUpdated] = data.proprietario;
        onHandleUpdateCostumer([...costumers]);
        handleCloseUpdate();
      }

      toast.success('Cliente atualizado com sucesso!');

      setLoading(false);
    } catch (err) {
      console.log('err', err.response);
      setLoading(false);

      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else if (err.message) {
        toast.error(err.message);
      }
    }
  }, [
    phone, name, obs, setLoading, costumerSelected, onHandleUpdateCostumer, costumers,
    handleCloseUpdate, checkChanges,
  ]);

  return (
    <form className={styles.form} onSubmit={handleUpdateUser}>
      <button
        type="button"
        className={styles.back}
        onClick={handleCloseUpdate}
      >
        <FiArrowLeft />
      </button>

      <label htmlFor="nome">Nome</label>
      <input
        type="text"
        id="nome"
        placeholder="Digite o nome"
        value={name}
        onChange={(e) => { setName(e.target.value); }}
      />

      <label htmlFor="telefone">Contato</label>
      <InputMask
        type="text"
        id="phone"
        mask="(99) 99999-9999"
        placeholder="Digite o número de contato"
        onChange={(e) => { setPhone(e.target.value); }}
      />

      <label htmlFor="obs">Observação</label>
      <input
        type="obs"
        id="obs"
        placeholder="Digite uma observação"
        value={obs}
        onChange={(e) => { setObs(e.target.value); }}
      />
      <button className={styles.buttonForm} type="submit">Atualizar</button>
    </form>
  );
}
