import React, { useCallback } from 'react';
import { InputSearch } from '@/components/InputSearch';
import styles from './styles.module.scss';

interface ICostumer {
    id_proprietario: string;
    nome: string;
    obs: string;
    contato: string;
}

interface IFilter {
    nome?: string;
    obs?: string;
    contato?: string;
}

interface ListCostumerProps {
    onHandleSelectACostumer: (costumer: ICostumer) => void;
    costumers: ICostumer[];
    filter: IFilter;
    onHandleFilter: (filter: IFilter) => void;
}

export function ListCostumer({
  costumers,
  filter,
  onHandleFilter,
  onHandleSelectACostumer,
}: ListCostumerProps): JSX.Element {
  const handleFilter = useCallback((filterObject: IFilter) => {
    if (filterObject.nome === '') {
      delete filter.nome;
      delete filterObject.nome;
    }

    onHandleFilter({
      ...filter,
      ...filterObject,
    });
  }, [onHandleFilter, filter]);

  return (
    <>
      <div className={styles.containerSearch}>
        <div>
          <label htmlFor="nome">Nome</label>
          <InputSearch
            placeholder="todos"
            id="nome"
            onChange={(e) => { handleFilter({ nome: e.target.value }); }}
          />
        </div>
      </div>
      <ul className={styles.list}>
        {costumers.length === 0
          ? (
            <li>
              <h1 className={styles.nenhumResultado}>Nenhum Resultado encontrado</h1>
            </li>
          )

          : costumers.map((costumerMapped) => (
            <li key={costumerMapped.id_proprietario}>
              <div>
                <p>
                  Nome:
                  {' '}
                  <span>{costumerMapped.nome}</span>
                </p>
                <p>
                  Contato:
                  {' '}
                  <span>{costumerMapped.contato}</span>
                </p>
                <p>
                  Observação:
                  {' '}
                  <span>{costumerMapped.obs}</span>
                </p>
              </div>
              <div className={styles.buttonContainer}>
                <button
                  type="button"
                  onClick={() => {
                    onHandleSelectACostumer(costumerMapped);
                  }}
                >
                  Editar
                </button>
              </div>
            </li>
          ))}
      </ul>
    </>
  );
}
