import { FormEvent, useCallback, useState } from 'react';
import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';

import { FiArrowLeft } from 'react-icons/fi';
import handleErrors from '@/utils/handleErrors';
import { useAuth } from '@/hooks/useAuth';
import styles from './styles.module.scss';

interface IUsers {
    id_usuario: string;
    login: string;
    nome: string;
    hierarquia: string;
    senha?: string;
}

interface UpdateUserProps {
    onHandleSelectAUser: (user: IUsers) => void;
    userSelected: IUsers;
    users: IUsers[];
    onHandleUpdateUser: (user: IUsers[]) => void;
}

export function UpdateUser({
  userSelected,
  onHandleSelectAUser,
  users,
  onHandleUpdateUser,
}: UpdateUserProps): JSX.Element {
  const { setLoading } = useLoading();
  const { signOut, usuario: userLogged } = useAuth();
  const [email, setEmail] = useState(userSelected.login);
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [name, setName] = useState(userSelected.nome);

  const checkChanges = useCallback((user: Omit<IUsers, 'id_usuario' | 'hierarquia' >): Promise<Omit<IUsers, 'id_usuario' | 'hierarquia' >> => new Promise(
    (resolve) => {
      if (user.login === userSelected.login) {
        delete user.login;
      }

      if (user.nome === userSelected.nome) {
        delete user.nome;
      }

      if (user.senha === '') {
        delete user.senha;
      }
      resolve(user);
    },
  ), [userSelected]);

  const handleCloseUpdate = useCallback(() => {
    onHandleSelectAUser({} as any);
  }, [onHandleSelectAUser]);

  const handleUpdateUser = useCallback(async () => {
    setLoading(true);
    try {
      if (password !== confirmPassword) {
        throw new Error('Campo "Senha" e "Confirmar senha" não contém os mesmos valores!');
      }

      const usuario = {
        login: email,
        nome: name,
        senha: password,
      };

      const checkedUser = await checkChanges(usuario);

      const { data } = await api.put(`/usuario/${userSelected.id_usuario}`, {
        usuario: checkedUser,
      });

      const findIndexUserUpdated = users.findIndex((userMapped) => (
        userMapped.id_usuario === data.usuario.id_usuario
      ));

      if (findIndexUserUpdated >= 0) {
        users[findIndexUserUpdated] = data.usuario;
        onHandleUpdateUser([...users]);
        handleCloseUpdate();
      }

      toast.success('Usuario atualizado com sucesso!');

      if (userLogged.id_usuario === userSelected.id_usuario) {
        signOut();
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);

      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else if (err.message) {
        toast.error(err.message);
      }
    }
  }, [
    email, name, password, confirmPassword, setLoading, userSelected, onHandleUpdateUser, users,
    handleCloseUpdate, signOut, checkChanges, userLogged,
  ]);

  const alert = useCallback(() => {
    confirmAlert({
      title: 'Alerta!',
      message: 'Para atualizar seu usuário será necessário logar novamente, tem certeza que deseja alterar seus dados?',
      buttons: [
        {
          label: 'Sim',
          onClick: () => {
            handleUpdateUser();
          },
        },
        {
          label: 'Não',
          onClick: () => { console.log('Faça Nada'); },
        },
      ],
    });
  }, [handleUpdateUser]);

  const checkIfIsUserLogger = useCallback(async (event: FormEvent) => {
    event.preventDefault();
    if (userLogged.id_usuario === userSelected.id_usuario) {
      alert();
    } else {
      await handleUpdateUser();
    }
  }, [alert, handleUpdateUser, userSelected, userLogged]);

  return (
    <form className={styles.form} onSubmit={checkIfIsUserLogger}>
      <button
        type="button"
        className={styles.back}
        onClick={handleCloseUpdate}
      >
        <FiArrowLeft />
      </button>

      <label htmlFor="email">E-mail</label>
      <input
        type="email"
        id="email"
        placeholder="Digite o E-mail"
        value={email}
        onChange={(e) => { setEmail(e.target.value); }}
      />

      <label htmlFor="nome">Nome</label>
      <input
        type="text"
        id="nome"
        placeholder="Digite o nome"
        value={name}
        onChange={(e) => { setName(e.target.value); }}
      />

      <label htmlFor="senha">Senha</label>
      <input
        type="password"
        id="senha"
        placeholder="Digite sua senha"
        value={password}
        onChange={(e) => { setPassword(e.target.value); }}
      />

      <label htmlFor="confirmar-senha">Confirmar senha</label>
      <input
        type="password"
        id="confirmar-senha"
        placeholder="Confirme a senha"
        value={confirmPassword}
        onChange={(e) => { setConfirmPassword(e.target.value); }}
      />

      <button className={styles.buttonForm} type="submit">Atualizar</button>
    </form>
  );
}
