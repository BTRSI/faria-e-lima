import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import handleErrors from '@/utils/handleErrors';
import { useCallback, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { CreateUser } from './CreateUser';
import { ListUser } from './ListUser';
import styles from './styles.module.scss';
import { UpdateUser } from './UpdateUser';

interface IUsers {
    id_usuario: string;
    login: string;
    nome: string;
    hierarquia: string;
}

interface IFilter {
    login?: string;
    nome?: string;
    hierarquia?: string;
    status?: string;
}

export function User(): JSX.Element {
  const { setLoading } = useLoading();
  const [userTypeSelected, setUserTypeSelected] = useState<'listar' | 'criar'>('listar');
  const [userSelected, setUserSelected] = useState<IUsers>({} as IUsers);
  const [filter, setFilter] = useState<IFilter>({
    status: 'ATIVO',
  } as IFilter);

  const [users, setUsers] = useState<IUsers[]>([]);

  useEffect(() => {
    setLoading(true);
    api.post('/usuario/pesquisa', {
      usuario: {
        ...filter,
      },
    })
      .then((response) => {
        setLoading(false);
        setUsers(response.data);
      })
      .catch((err) => {
        setLoading(false);
        setUsers([]);
        if (err.response) {
          const { message } = handleErrors(err.response);
          toast.error(message[0]);
        } else {
          toast.error('Erro ao buscar usuario');
        }
      });
  }, [setLoading, filter]);

  const handleOptionSelected = useCallback((option: 'listar' | 'criar') => {
    setUserTypeSelected(option);
  }, []);

  const handleUpdateUser = useCallback((user: IUsers[]) => {
    setUsers(user);
  }, []);

  const handleSelectAUser = useCallback((user: IUsers) => {
    setUserSelected(user);
  }, []);

  const handleFilter = useCallback((filterObject: IFilter) => {
    setFilter(filterObject);
  }, []);

  return (
    <>
      <div className={styles.containerSelect}>
        <nav>
          <button type="button" className={userTypeSelected === 'listar' ? `${styles.active}` : ''} onClick={() => { handleOptionSelected('listar'); }}>Todos</button>
        </nav>
        <nav>
          <button type="button" className={userTypeSelected === 'criar' ? `${styles.active}` : ''} onClick={() => { handleOptionSelected('criar'); }}>Cadastrar</button>
        </nav>
      </div>
      <div className={styles.content}>
        {(userTypeSelected === 'listar' && !userSelected.id_usuario)
            && (
            <ListUser
              onHandleSelectAUser={handleSelectAUser}
              users={users}
              onHandleUpdateUser={handleUpdateUser}
              filter={filter}
              onHandleFilter={handleFilter}
            />
            )}

        {(userTypeSelected === 'listar' && userSelected.id_usuario)
            && (
            <UpdateUser
              onHandleSelectAUser={handleSelectAUser}
              userSelected={userSelected}
              onHandleUpdateUser={handleUpdateUser}
              users={users}
            />
            )}

        {userTypeSelected === 'criar'
                && (
                <CreateUser
                  onHandleOptionSelected={handleOptionSelected}
                  onHandleUpdateUser={handleUpdateUser}
                  users={users}
                />
                )}
      </div>
    </>
  );
}
