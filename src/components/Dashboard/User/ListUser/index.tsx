import { confirmAlert } from 'react-confirm-alert';
import { useAuth } from '@/hooks/useAuth';
import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import handleErrors from '@/utils/handleErrors';
import { useCallback } from 'react';
import { toast } from 'react-toastify';
import { InputSearch } from '@/components/InputSearch';
import styles from './styles.module.scss';

interface IUsers {
    id_usuario: string;
    login: string;
    nome: string;
    hierarquia: string;
}

interface IFilter {
    login?: string;
    nome?: string;
    status?: string;
}

interface ListUserProps {
    onHandleSelectAUser: (user: IUsers) => void;
    users: IUsers[];
    onHandleUpdateUser: (user: IUsers[]) => void;
    filter: IFilter;
    onHandleFilter: (filter: IFilter) => void;
}

export function ListUser({
  onHandleSelectAUser,
  users,
  onHandleUpdateUser,
  filter,
  onHandleFilter,
}: ListUserProps): JSX.Element {
  const { setLoading } = useLoading();
  const { usuario: userLogged } = useAuth();

  const handleDisabledUser = useCallback(async (id: string) => {
    setLoading(true);
    try {
      if (userLogged.id_usuario === id) {
        throw new Error('Usuário não pode inativar a si mesmo!');
      }

      const { data } = await api.put(`/usuario/${id}`, {
        usuario: {
          status: 'INATIVO',
        },
      });

      const userFiltered = users.filter((userMapped) => (
        userMapped.id_usuario !== data.usuario.id_usuario
      ));

      onHandleUpdateUser([...userFiltered]);

      setLoading(false);
      toast.success('Usuario inativado com sucesso!');
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else if (err.message) {
        toast.error(err.message);
      }
    }
  }, [setLoading, userLogged, onHandleUpdateUser, users]);

  const alert = useCallback((id: string) => {
    confirmAlert({
      title: 'Alerta!',
      message: 'Tem certeza que deseja excluir o usuario?',
      buttons: [
        {
          label: 'Sim',
          onClick: () => {
            handleDisabledUser(id);
          },
        },
        {
          label: 'Não',
          onClick: () => { console.log('Faça nada'); },
        },
      ],
    });
  }, [handleDisabledUser]);

  const handleFilter = useCallback((filterObject: IFilter) => {
    if (filterObject.nome === '') {
      delete filter.nome;
      delete filterObject.nome;
    }

    if (filterObject.login === '') {
      delete filter.login;
      delete filterObject.login;
    }

    onHandleFilter({
      ...filter,
      ...filterObject,
    });
  }, [onHandleFilter, filter]);

  return (
    <>
      <div className={styles.containerSearch}>
        <div>
          <label htmlFor="nome">Nome</label>
          <InputSearch
            placeholder="todos"
            id="nome"
            onChange={(e) => { handleFilter({ nome: e.target.value }); }}
          />
        </div>
        <div>
          <label htmlFor="email">E-mail</label>
          <InputSearch
            placeholder="todos"
            id="email"
            onChange={(e) => { handleFilter({ login: e.target.value }); }}
          />
        </div>
      </div>

      <ul className={styles.list}>
        {users.length === 0
          ? (
            <li>
              <h1 className={styles.nenhumResultado}>Nenhum Resultado encontrado</h1>
            </li>
          )

          : users.map((userMapped) => (
            <li key={userMapped.id_usuario}>
              <div>
                <p>
                  Nome:
                  {' '}
                  <span>{userMapped.nome}</span>
                </p>
                <p>
                  E-mail:
                  {' '}
                  <span>{userMapped.login}</span>
                </p>
              </div>
              <div className={styles.buttonContainer}>
                <button type="button" onClick={() => { onHandleSelectAUser(userMapped); }}>Editar</button>
                <button type="button" onClick={() => { alert(userMapped.id_usuario); }}>Excluir</button>
              </div>
            </li>
          ))}
      </ul>
    </>
  );
}
