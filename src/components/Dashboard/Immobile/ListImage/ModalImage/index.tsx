import {
  ChangeEvent, useCallback, useEffect, useMemo, useState,
} from 'react';
import { FiEdit2, FiX } from 'react-icons/fi';
import imageCompression from 'browser-image-compression';
import { useLoading } from '@/hooks/useLoading';
import handleErrors from '@/utils/handleErrors';
import { toast } from 'react-toastify';
import { api } from '@/services/axios';
import styles from './styles.module.scss';

interface IImage {
  file: string;
  id_imovel_imagem: string;
  order: string;
  destaque: string;
  titulo: string;
}

interface ModalImageProps {
  toggleModal: () => void;
  imageSelected: IImage;
  immobileId: string;
  onHandleAddImage: (image: any) => void;
}

interface IFile {
  newImage: File;
}

export function ModalImage({
  toggleModal,
  imageSelected,
  immobileId,
  onHandleAddImage,
}: ModalImageProps) {
  const { setLoading } = useLoading();
  const [preview, setPreview] = useState('');
  const [urlPreview, setUrlPreview] = useState('');
  const [newFile, setNewFile] = useState<IFile>({} as IFile);

  useEffect(() => {
    if (imageSelected.id_imovel_imagem) {
      setUrlPreview(`http://mobi.tieng.com.br/imagens/${immobileId}/`);
      setPreview(imageSelected.file);
    }
  }, [imageSelected.file, imageSelected.id_imovel_imagem, immobileId]);

  const optionsImage = useMemo(() => ({
    maxSizeMB: 0.1,
    maxWidthOrHeight: 800,
  }), []);

  const removeCache = useMemo(() => {
    const date = new Date();
    return `?${date}`;
  }, []);

  const handlePreview = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (!file) {
      setPreview(null);
    }
    setNewFile({ newImage: file });
    setUrlPreview('');
    setUrlPreview('');
    const previewURL = URL.createObjectURL(file);

    setPreview(previewURL);
  }, []);

  const addNewImage = useCallback(async () => {
    setLoading(true);
    try {
      if (!newFile.newImage) {
        throw new Error('Nenhuma imagem selecionada!');
      }
      const compressFile = await imageCompression(newFile.newImage, optionsImage);
      const formData = new FormData();

      formData.append('imagem', compressFile);

      let response;

      if (imageSelected.id_imovel_imagem) {
        response = await api.put(`imovel/${immobileId}/imagem/${imageSelected.id_imovel_imagem}`, formData);
      } else {
        response = await api.post(`imovel/${immobileId}/imagem`, formData);
      }

      onHandleAddImage([...response.data.imagens]);
      toggleModal();
      toast.success('Sucesso ao adicionar imagem');

      setLoading(false);
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else if (err.message) {
        toast.error(err.message);
      } else {
        toast.error('Erro ao carregar imagem!');
      }
    }
  }, [imageSelected.id_imovel_imagem, immobileId,
    newFile.newImage, onHandleAddImage, optionsImage, setLoading, toggleModal]);

  return (
    <div className={styles.container}>
      <div className={styles.modalContainer}>
        <h1>Adicionar imagem</h1>
        <button
          type="button"
          className={styles.close}
          onClick={toggleModal}
        >
          <FiX size={18} />
        </button>
        <label htmlFor={`imovel-image-${imageSelected.id_imovel_imagem}`}>Imagem</label>
        <div className={styles.imageContainer}>
          { preview && (
          <img
            src={urlPreview !== ''
              ? `${urlPreview}${preview}${removeCache}`
              : `${urlPreview}${preview}`}
            alt="Preview"
          />
          ) }
          <label htmlFor={`imovel-image-${imageSelected.id_imovel_imagem}`}>
            <FiEdit2 />
            <input
              type="file"
              onChange={handlePreview}
              id={`imovel-image-${imageSelected.id_imovel_imagem}`}
              style={{ marginBottom: 8 }}
            />
          </label>
        </div>
        <button className={styles.addBtn} type="button" onClick={addNewImage}>
          {imageSelected.id_imovel_imagem ? 'Atualizar imagem' : 'Adicionar imagem'}
        </button>
      </div>
    </div>
  );
}
