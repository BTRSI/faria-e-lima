import {
  FormEvent, useCallback, useEffect, useState,
} from 'react';
import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import { toast } from 'react-toastify';

import handleErrors from '@/utils/handleErrors';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { ufList } from '@/constants/UF';
import styles from './styles.module.scss';

interface CreateImmobileProps {
    onHandleOptionSelected: (option: 'listar' | 'criar') => void;
    onHandleUpdateImmobile: (immobile: any) => void;
    immobiles: any[];
}

interface IImmobile {
    titulo: string;
    descricao: string;
    valor: string;
    comodos: string;
    dimensao: string;
    garagem: string;
    coordenadas: string;
    link_maps: string;
    rua: string;
    numero: string;
    bairro: string;
    id_proprietario_fk: string;
    id_modalidade_fk: string;
    id_categoria_fk: string;
    id_cidade_fk: string;
}

interface ICostumer {
    id_proprietario: string;
    nome: string;
}

interface IModality {
    id_modalidade: string;
    titulo: string;
}

interface ICategory {
    id_categoria: string;
    titulo: string;
}

interface ICity {
    id: string;
    nome: string;
}

export function CreateImmobile({
  onHandleOptionSelected,
  onHandleUpdateImmobile,
  immobiles,
}: CreateImmobileProps): JSX.Element {
  const { setLoading } = useLoading();
  const [data, setData] = useState<IImmobile>({} as IImmobile);
  const [costumers, setCostumers] = useState<ICostumer[]>([]);
  const [modalities, setModalities] = useState<IModality[]>([]);
  const [categories, setCategories] = useState<ICategory[]>([]);
  const [cities, setCities] = useState<ICity[]>([]);
  const [ufSelected, setUfSelected] = useState('');

  useEffect(() => {
    async function getInformations() {
      setLoading(true);

      const responseCostumer = await api.post('/proprietario/pesquisa', { proprietario: { status: 'ATIVO' } });
      const responseModality = await api.post('/modalidade/pesquisa', { modalidade: { status: 'ATIVO' } });
      const responseCateogry = await api.post('/categoria/pesquisa', { categoria: { status: 'ATIVO' } });

      setCostumers(responseCostumer.data.result);
      setModalities(responseModality.data.result);
      setCategories(responseCateogry.data.result);
      setLoading(false);
    }

    getInformations();
  }, [setLoading]);

  useEffect(() => {
    if (ufSelected !== '') {
      setLoading(true);

      api.post('/cidade/pesquisa', {
        cidade: {
          uf: ufSelected,
        },
      }).then((response) => {
        setLoading(false);
        setCities(response.data);
      }).catch(() => {
        setLoading(false);
        setCities([]);
      });
    }
  }, [ufSelected, setLoading]);

  const handleCreateImmobile = useCallback(async (event: FormEvent) => {
    event.preventDefault();
    setLoading(true);
    try {
      const response = await api.post('/imovel', {
        imovel: {
          ...data,
        },
      });

      onHandleOptionSelected('listar');

      onHandleUpdateImmobile([...immobiles, {
        ...response.data.imovel,
        valorFormatado: formatCurrencyToBrazil(Number(response.data.imovel.valor)),
      }]);

      toast.success('Imóvel cadastrado com sucesso!');

      setLoading(false);
    } catch (err) {
      console.log('err', err.message);
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      }
    }
  }, [
    data, setLoading, immobiles,
    onHandleOptionSelected, onHandleUpdateImmobile,
  ]);

  return (
    <form className={styles.form} onSubmit={handleCreateImmobile}>
      <label htmlFor="titulo">Titulo</label>
      <input
        type="text"
        id="titulo"
        placeholder="Digite o titulo"
        value={data.titulo}
        onChange={(e) => {
          setData({
            ...data,
            titulo: e.target.value,
          });
        }}
      />

      <label htmlFor="descricao">Descrição</label>
      <input
        type="text"
        id="descricao"
        placeholder="Digite a descricao"
        value={data.descricao}
        onChange={(e) => {
          setData({
            ...data,
            descricao: e.target.value,
          });
        }}
      />

      <label htmlFor="valor">Valor</label>
      <input
        type="text"
        id="valor"
        placeholder="Digite o valor"
        value={data.valor}
        onChange={(e) => {
          setData({
            ...data,
            valor: e.target.value,
          });
        }}
      />
      <label htmlFor="comodos">Comodos</label>
      <input
        type="text"
        id="comodos"
        placeholder="Digite quantidade de comodos"
        value={data.comodos}
        onChange={(e) => {
          setData({
            ...data,
            comodos: e.target.value,
          });
        }}
      />

      <label htmlFor="dimensao">Metros quadrados</label>
      <input
        type="text"
        id="dimensao"
        placeholder="Digite a quantidade de metros quadrados"
        value={data.dimensao}
        onChange={(e) => {
          setData({
            ...data,
            dimensao: e.target.value,
          });
        }}
      />

      <label htmlFor="garagem">Garagem</label>
      <select
        id="garagem"
        onChange={(e) => {
          setData({
            ...data,
            garagem: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        <option value="SIM">SIM</option>
        <option value="NAO">NÃO</option>
      </select>

      <label htmlFor="coordenadas">Coordenadas</label>
      <input
        type="text"
        id="coordenadas"
        placeholder="Digite as coordenadas"
        value={data.coordenadas}
        onChange={(e) => {
          setData({
            ...data,
            coordenadas: e.target.value,
          });
        }}
      />

      <label htmlFor="link_maps">Link para o Mapa</label>
      <input
        type="text"
        id="link_maps"
        placeholder="Digite as link do mapa"
        value={data.link_maps}
        onChange={(e) => {
          setData({
            ...data,
            link_maps: e.target.value,
          });
        }}
      />

      <label htmlFor="uf">UF</label>
      <select
        id="uf"
        onChange={(e) => { setUfSelected(e.target.value); }}
      >
        <option value="">Selecione</option>
        {ufList.map((ufMapped) => (
          <option key={ufMapped.value} value={ufMapped.value}>{ufMapped.label}</option>
        ))}
      </select>

      <label htmlFor="cidade">Cidade</label>
      <select
        id="cidade"
        onChange={(e) => {
          setData({
            ...data,
            id_cidade_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {cities.map((cityMapped) => (
          <option key={cityMapped.id} value={cityMapped.id}>{cityMapped.nome}</option>
        ))}
      </select>

      <label htmlFor="rua">Endereço</label>
      <input
        type="text"
        id="rua"
        placeholder="Digite o endereço"
        value={data.rua}
        onChange={(e) => {
          setData({
            ...data,
            rua: e.target.value,
          });
        }}
      />

      <label htmlFor="numero">Numero</label>
      <input
        type="text"
        id="numero"
        placeholder="Digite o numero"
        value={data.numero}
        onChange={(e) => {
          setData({
            ...data,
            numero: e.target.value,
          });
        }}
      />

      <label htmlFor="bairro">Bairro</label>
      <input
        type="text"
        id="bairro"
        placeholder="Digite o bairro"
        value={data.bairro}
        onChange={(e) => {
          setData({
            ...data,
            bairro: e.target.value,
          });
        }}
      />

      <label htmlFor="cliente">Cliente</label>
      <select
        id="cliente"
        onChange={(e) => {
          setData({
            ...data,
            id_proprietario_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {costumers.map((costumerMapped) => (
          <option
            key={costumerMapped.id_proprietario}
            value={costumerMapped.id_proprietario}
          >
            {costumerMapped.nome}

          </option>
        ))}
      </select>

      <label htmlFor="modalidade">Tipo</label>
      <select
        id="modalidade"
        onChange={(e) => {
          setData({
            ...data,
            id_modalidade_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {modalities.map((modalityMapped) => (
          <option
            key={modalityMapped.id_modalidade}
            value={modalityMapped.id_modalidade}
          >
            {modalityMapped.titulo}

          </option>
        ))}
      </select>

      <label htmlFor="categoria">Categoria</label>
      <select
        id="categoria"
        onChange={(e) => {
          setData({
            ...data,
            id_categoria_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {categories.map((categoryMapped) => (
          <option
            key={categoryMapped.id_categoria}
            value={categoryMapped.id_categoria}
          >
            {categoryMapped.titulo}

          </option>
        ))}
      </select>

      <button type="submit">Cadastrar</button>
    </form>
  );
}
