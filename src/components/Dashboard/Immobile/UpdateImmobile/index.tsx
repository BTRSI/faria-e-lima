import {
  FormEvent, useCallback, useEffect, useState,
} from 'react';
import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import { toast } from 'react-toastify';

import { FiArrowLeft } from 'react-icons/fi';
import handleErrors from '@/utils/handleErrors';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { ufList } from '@/constants/UF';
import styles from './styles.module.scss';

interface IImmobile {
    id_imovel?: string;
    titulo: string;
    descricao: string;
    valor: string;
    comodos: string;
    dimensao: string;
    garagem: string;
    coordenadas: string;
    link_maps: string;
    rua: string;
    numero: string;
    bairro: string;
    id_proprietario_fk: string;
    id_modalidade_fk: string;
    id_categoria_fk: string;
    id_cidade_fk: string;
}

interface ICostumer {
    id_proprietario: string;
    nome: string;
}

interface IModality {
    id_modalidade: string;
    titulo: string;
}

interface ICategory {
    id_categoria: string;
    titulo: string;
}

interface ICity {
    id: string;
    nome: string;
    uf: string;
}

interface UpdateImmobileProps {
    onHandleSelectAImmobile: (immobile: IImmobile) => void;
    immobileSelected: IImmobile;
    immobiles: IImmobile[];
    onHandleUpdateImmobile: (immobile: IImmobile[]) => void;
}

export function UpdateImmobile({
  immobileSelected,
  onHandleSelectAImmobile,
  immobiles,
  onHandleUpdateImmobile,
}: UpdateImmobileProps): JSX.Element {
  const { setLoading } = useLoading();
  const [data, setData] = useState<IImmobile>({
    bairro: immobileSelected.bairro,
    comodos: immobileSelected.comodos,
    coordenadas: immobileSelected.coordenadas,
    descricao: immobileSelected.descricao,
    dimensao: immobileSelected.dimensao,
    garagem: immobileSelected.garagem,
    id_categoria_fk: immobileSelected.id_categoria_fk,
    id_cidade_fk: immobileSelected.id_cidade_fk,
    id_modalidade_fk: immobileSelected.id_modalidade_fk,
    id_proprietario_fk: immobileSelected.id_proprietario_fk,
    link_maps: immobileSelected.link_maps,
    numero: immobileSelected.numero,
    rua: immobileSelected.rua,
    titulo: immobileSelected.titulo,
    valor: immobileSelected.valor,
  });
  const [costumers, setCostumers] = useState<ICostumer[]>([]);
  const [modalities, setModalities] = useState<IModality[]>([]);
  const [categories, setCategories] = useState<ICategory[]>([]);
  const [cities, setCities] = useState<ICity[]>([]);
  const [ufSelected, setUfSelected] = useState('');

  useEffect(() => {
    async function getInformations() {
      setLoading(true);

      const responseCostumer = await api.post('/proprietario/pesquisa', { proprietario: { status: 'ATIVO' } });
      const responseModality = await api.post('/modalidade/pesquisa', { modalidade: { status: 'ATIVO' } });
      const responseCateogry = await api.post('/categoria/pesquisa', { categoria: { status: 'ATIVO' } });
      const responseCitySelected = await api.get(`/cidade/${immobileSelected.id_cidade_fk}`);

      setCostumers(responseCostumer.data.result);
      setModalities(responseModality.data.result);
      setCategories(responseCateogry.data.result);
      setUfSelected(responseCitySelected.data.uf);
      setLoading(false);
    }

    getInformations();
  }, [immobileSelected, setLoading]);

  useEffect(() => {
    if (ufSelected !== '') {
      setLoading(true);

      api.post('/cidade/pesquisa', {
        cidade: {
          uf: ufSelected,
        },
      }).then((response) => {
        setLoading(false);
        setCities(response.data);
      }).catch(() => {
        setLoading(false);
        setCities([]);
      });
    }
  }, [ufSelected, setLoading]);

  const checkChanges = useCallback((immobile: IImmobile): Promise<IImmobile> => new Promise((
    resolve,
  ) => {
    if (immobile.titulo === immobileSelected.titulo) {
      delete immobile.titulo;
    }

    if (immobile.bairro === immobileSelected.bairro) {
      delete immobile.bairro;
    }

    if (immobile.comodos === immobileSelected.comodos) {
      delete immobile.comodos;
    }

    if (immobile.coordenadas === immobileSelected.coordenadas) {
      delete immobile.coordenadas;
    }

    if (immobile.descricao === immobileSelected.descricao) {
      delete immobile.descricao;
    }

    if (immobile.dimensao === immobileSelected.dimensao) {
      delete immobile.dimensao;
    }

    if (immobile.garagem === immobileSelected.garagem) {
      delete immobile.garagem;
    }

    if (immobile.id_categoria_fk === immobileSelected.id_categoria_fk) {
      delete immobile.id_categoria_fk;
    }

    if (immobile.id_cidade_fk === immobileSelected.id_cidade_fk) {
      delete immobile.id_cidade_fk;
    }

    if (immobile.id_modalidade_fk === immobileSelected.id_modalidade_fk) {
      delete immobile.id_modalidade_fk;
    }

    if (immobile.id_proprietario_fk === immobileSelected.id_proprietario_fk) {
      delete immobile.id_proprietario_fk;
    }

    if (immobile.link_maps === immobileSelected.link_maps) {
      delete immobile.link_maps;
    }

    if (immobile.numero === immobileSelected.numero) {
      delete immobile.numero;
    }

    if (immobile.rua === immobileSelected.rua) {
      delete immobile.rua;
    }

    if (immobile.valor === immobileSelected.valor) {
      delete immobile.valor;
    }

    resolve(immobile);
  }), [immobileSelected]);

  const handleCloseUpdate = useCallback(() => {
    onHandleSelectAImmobile({} as any);
  }, [onHandleSelectAImmobile]);

  const handleUpdateImmobile = useCallback(async (event: FormEvent) => {
    event.preventDefault();
    setLoading(true);
    try {
      const imovel = { ...data };

      const checkedimmobile = await checkChanges(imovel);

      const response = await api.put(`/imovel/${immobileSelected.id_imovel}`, {
        imovel: checkedimmobile,
      });

      const findIndexImmobileUpdated = immobiles.findIndex((immobileMapped) => (
        immobileMapped.id_imovel === response.data.imovel.id_imovel
      ));

      if (findIndexImmobileUpdated >= 0) {
        immobiles[findIndexImmobileUpdated] = {
          ...response.data.imovel,
          valorFormatado: formatCurrencyToBrazil(Number(response.data.imovel.valor)),
        };
        onHandleUpdateImmobile([...immobiles]);
        handleCloseUpdate();
      }

      toast.success('Imóvel atualizado com sucesso!');

      setLoading(false);
    } catch (err) {
      console.log('err', err.response);
      setLoading(false);

      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      }
    }
  }, [
    data, setLoading, immobileSelected, onHandleUpdateImmobile, immobiles,
    handleCloseUpdate, checkChanges,
  ]);

  return (
    <form className={styles.form} onSubmit={handleUpdateImmobile}>
      <button
        type="button"
        className={styles.back}
        onClick={handleCloseUpdate}
      >
        <FiArrowLeft />
      </button>

      <label htmlFor="titulo">Titulo</label>
      <input
        type="text"
        id="titulo"
        placeholder="Digite o titulo"
        value={data.titulo}
        onChange={(e) => {
          setData({
            ...data,
            titulo: e.target.value,
          });
        }}
      />

      <label htmlFor="descricao">Descrição</label>
      <input
        type="text"
        id="descricao"
        placeholder="Digite a descricao"
        value={data.descricao}
        onChange={(e) => {
          setData({
            ...data,
            descricao: e.target.value,
          });
        }}
      />

      <label htmlFor="valor">Valor</label>
      <input
        type="text"
        id="valor"
        placeholder="Digite o valor"
        value={data.valor}
        onChange={(e) => {
          setData({
            ...data,
            valor: e.target.value,
          });
        }}
      />
      <label htmlFor="comodos">Comodos</label>
      <input
        type="text"
        id="comodos"
        placeholder="Digite quantidade de comodos"
        value={data.comodos}
        onChange={(e) => {
          setData({
            ...data,
            comodos: e.target.value,
          });
        }}
      />

      <label htmlFor="dimensao">Metros quadrados</label>
      <input
        type="text"
        id="dimensao"
        placeholder="Digite a quantidade de metros quadrados"
        value={data.dimensao}
        onChange={(e) => {
          setData({
            ...data,
            dimensao: e.target.value,
          });
        }}
      />

      <label htmlFor="garagem">Garagem</label>
      <select
        id="garagem"
        value={data.garagem}
        onChange={(e) => {
          setData({
            ...data,
            garagem: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        <option value="SIM">SIM</option>
        <option value="NAO">NÃO</option>
      </select>

      <label htmlFor="coordenadas">Coordenadas</label>
      <input
        type="text"
        id="coordenadas"
        placeholder="Digite as coordenadas"
        value={data.coordenadas}
        onChange={(e) => {
          setData({
            ...data,
            coordenadas: e.target.value,
          });
        }}
      />

      <label htmlFor="link_maps">Link para o Mapa</label>
      <input
        type="text"
        id="link_maps"
        placeholder="Digite as link do mapa"
        value={data.link_maps}
        onChange={(e) => {
          setData({
            ...data,
            link_maps: e.target.value,
          });
        }}
      />

      <label htmlFor="uf">UF</label>
      <select
        id="uf"
        value={ufSelected}
        onChange={(e) => { setUfSelected(e.target.value); }}
      >
        <option value="">Selecione</option>
        {ufList.map((ufMapped) => (
          <option key={ufMapped.value} value={ufMapped.value}>{ufMapped.label}</option>
        ))}
      </select>

      <label htmlFor="cidade">Cidade</label>
      <select
        id="cidade"
        value={data.id_cidade_fk}
        onChange={(e) => {
          setData({
            ...data,
            id_cidade_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {cities.map((cityMapped) => (
          <option key={cityMapped.id} value={cityMapped.id}>{cityMapped.nome}</option>
        ))}
      </select>

      <label htmlFor="rua">Endereço</label>
      <input
        type="text"
        id="rua"
        placeholder="Digite o endereço"
        value={data.rua}
        onChange={(e) => {
          setData({
            ...data,
            rua: e.target.value,
          });
        }}
      />

      <label htmlFor="numero">Numero</label>
      <input
        type="text"
        id="numero"
        placeholder="Digite o numero"
        value={data.numero}
        onChange={(e) => {
          setData({
            ...data,
            numero: e.target.value,
          });
        }}
      />

      <label htmlFor="bairro">Bairro</label>
      <input
        type="text"
        id="bairro"
        placeholder="Digite o bairro"
        value={data.bairro}
        onChange={(e) => {
          setData({
            ...data,
            bairro: e.target.value,
          });
        }}
      />

      <label htmlFor="cliente">Cliente</label>
      <select
        id="cliente"
        value={data.id_proprietario_fk}
        onChange={(e) => {
          setData({
            ...data,
            id_proprietario_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {costumers.map((costumerMapped) => (
          <option
            key={costumerMapped.id_proprietario}
            value={costumerMapped.id_proprietario}
          >
            {costumerMapped.nome}

          </option>
        ))}
      </select>

      <label htmlFor="modalidade">Tipo</label>
      <select
        id="modalidade"
        value={data.id_modalidade_fk}
        onChange={(e) => {
          setData({
            ...data,
            id_modalidade_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {modalities.map((modalityMapped) => (
          <option
            key={modalityMapped.id_modalidade}
            value={modalityMapped.id_modalidade}
          >
            {modalityMapped.titulo}

          </option>
        ))}
      </select>

      <label htmlFor="categoria">Categoria</label>
      <select
        id="categoria"
        value={data.id_categoria_fk}
        onChange={(e) => {
          setData({
            ...data,
            id_categoria_fk: e.target.value,
          });
        }}
      >
        <option value="">Selecione</option>
        {categories.map((categoryMapped) => (
          <option
            key={categoryMapped.id_categoria}
            value={categoryMapped.id_categoria}
          >
            {categoryMapped.titulo}

          </option>
        ))}
      </select>
      <button className={styles.buttonForm} type="submit">Atualizar</button>
    </form>
  );
}
