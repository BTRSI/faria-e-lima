import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import handleErrors from '@/utils/handleErrors';
import React, { useCallback, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { CreateImmobile } from './CreateImmobile';
import { ListImmobile } from './ListImmobile';
import { UpdateImmobile } from './UpdateImmobile';
import styles from './styles.module.scss';
import { ListImage } from './ListImage';

interface IFilter {
    titulo?: string;
    valor?: string;
    id_proprietario_fk?: string;
    status?: string;
}

export function Immobile(): JSX.Element {
  const { setLoading } = useLoading();

  const [immobileTypeSelected, setImmobileTypeSelected] = useState<'listar' | 'criar' | 'imagem'>('listar');
  const [immobileSelected, setImmobileSelected] = useState<any>({} as any);
  const [filter, setFilter] = useState<IFilter>({
    status: 'ATIVO',
  } as IFilter);

  const [immobiles, setImmobiles] = useState<any[]>([]);

  const handleOptionSelected = useCallback((option: 'listar' | 'criar') => {
    setImmobileTypeSelected(option);
  }, []);

  useEffect(() => {
    setLoading(true);
    api.post('/imovel/pesquisa', {
      imovel: {
        ...filter,
      },
    })
      .then((response) => {
        setLoading(false);
        setImmobiles(response.data.result.map((immobileMapped) => ({
          ...immobileMapped,
          valorFormatado: formatCurrencyToBrazil(Number(immobileMapped.valor)),
        })));
      })
      .catch((err) => {
        console.error(err);
        setLoading(false);
        setImmobiles([]);
        if (err.response) {
          const { message } = handleErrors(err.response);
          toast.error(message[0]);
        } else {
          toast.error('Erro ao buscar imóveis');
        }
      });
  }, [setLoading, filter]);

  const handleUpdateImmobile = useCallback((immobile: any[]) => {
    setImmobiles(immobile);
  }, []);

  const handleSelectAImmobile = useCallback((immobile: any) => {
    setImmobileSelected(immobile);
  }, []);

  const handleFilter = useCallback((filterObject: any[]) => {
    setImmobiles(filterObject);
  }, []);

  return (
    <>
      <div className={styles.containerSelect}>
        <nav>
          <button type="button" className={immobileTypeSelected === 'listar' ? `${styles.active}` : ''} onClick={() => { handleOptionSelected('listar'); }}>Todos</button>
        </nav>
        <nav>
          <button type="button" className={immobileTypeSelected === 'criar' ? `${styles.active}` : ''} onClick={() => { handleOptionSelected('criar'); }}>Cadastrar</button>
        </nav>
      </div>
      <div className={styles.content}>
        {(immobileTypeSelected === 'listar' && !immobileSelected.id_imovel)
            && (
            <ListImmobile
              onHandleSelectImmobile={handleSelectAImmobile}
              immobiles={immobiles}
              filter={filter}
              onHandleFilter={handleFilter}
              onHandleOptionSelected={handleOptionSelected}
              onHandleUpdateImmobile={handleUpdateImmobile}
            />
            )}

        {(immobileTypeSelected === 'listar' && immobileSelected.id_imovel)
                && (
                <UpdateImmobile
                  onHandleSelectAImmobile={handleSelectAImmobile}
                  immobileSelected={immobileSelected}
                  onHandleUpdateImmobile={handleUpdateImmobile}
                  immobiles={immobiles}
                />
                )}

        {immobileTypeSelected === 'criar'
                && (
                <CreateImmobile
                  onHandleOptionSelected={handleOptionSelected}
                  onHandleUpdateImmobile={handleUpdateImmobile}
                  immobiles={immobiles}
                />
                )}

        {immobileTypeSelected === 'imagem'
                && (
                <ListImage
                  onHandleOptionSelected={handleOptionSelected}
                  immobile={immobileSelected}
                  onHandleSelectAImmobile={handleSelectAImmobile}
                />
                )}
      </div>
    </>
  );
}
