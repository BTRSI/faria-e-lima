import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import handleErrors from '@/utils/handleErrors';
import { useCallback } from 'react';
import { toast } from 'react-toastify';
import { InputSearch } from '@/components/InputSearch';
import { SearchImmobile } from '@/components/SearchImmobile';
import styles from './styles.module.scss';

interface IImmobile{
    id_imovel: string;
    titulo: string;
    valor: string;
    id_proprietario_fk: string;
    valorFormatado: string;
}

interface IFilter {
    titulo?: string;
    valor?: string;
    id_proprietario_fk?: string;
    status?: string;
}

interface ListImmobileProps {
    onHandleSelectImmobile: (Immobile: IImmobile) => void;
    immobiles: IImmobile[];
    filter: IFilter;
    onHandleFilter: (filter: any[]) => void;
    onHandleUpdateImmobile:(immobile: IImmobile[]) => void;
    onHandleOptionSelected:(option: 'listar' | 'criar' | 'imagem') => void;
}

export function ListImmobile({
  immobiles,
  onHandleSelectImmobile,
  onHandleUpdateImmobile,
  onHandleFilter,
  onHandleOptionSelected,
}: ListImmobileProps): JSX.Element {
  const { setLoading } = useLoading();

  const handleDisabledUser = useCallback(async (id: string) => {
    setLoading(true);
    try {
      const { data } = await api.put(`/imovel/${id}`, {
        imovel: {
          status: 'INATIVO',
        },
      });

      const immobileFiltered = immobiles.filter((immobileMapped) => (
        immobileMapped.id_imovel !== data.result.id_imovel
      ));

      onHandleUpdateImmobile([...immobileFiltered]);

      setLoading(false);
      toast.success('Cliente inativado com sucesso!');
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      }
    }
  }, [setLoading, onHandleUpdateImmobile, immobiles]);

  return (
    <>
      <div className={styles.containerSearch}>
        <SearchImmobile onHandleSearchToImmobile={onHandleFilter} />
      </div>
      <ul className={styles.list}>
        {immobiles.length === 0
          ? (
            <li>
              <h1 className={styles.nenhumResultado}>Nenhum Resultado encontrado</h1>
            </li>
          )

          : immobiles.map((immobileMapped) => (
            <li key={immobileMapped.id_imovel}>
              <div>
                <p>
                  Código:
                  {' '}
                  <span>
                    #
                    {immobileMapped.id_imovel}
                  </span>
                </p>
                <p>
                  Nome:
                  {' '}
                  <span>{immobileMapped.titulo}</span>
                </p>
                <p>
                  Valor:
                  {' '}
                  <span>{immobileMapped.valorFormatado}</span>
                </p>
              </div>
              <div className={styles.buttonContainer}>
                <button type="button" onClick={() => { onHandleSelectImmobile(immobileMapped); }}>
                  Detalhes
                </button>
                <button
                  type="button"
                  onClick={() => {
                    onHandleSelectImmobile(immobileMapped);
                    onHandleOptionSelected('imagem');
                  }}
                >
                  Imagens
                </button>
                <button type="button" onClick={() => { handleDisabledUser(immobileMapped.id_imovel); }}>Excluir</button>
              </div>
            </li>
          ))}
      </ul>
    </>
  );
}
