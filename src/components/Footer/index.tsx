import { FiPhone, FiMapPin, FiMail } from 'react-icons/fi';
import { FaWhatsapp } from 'react-icons/fa';
import styles from './styles.module.scss';

export default function Footer(): JSX.Element {
  return (
    <footer className={styles.footerContainer}>
      <div className={styles.footerContent}>
        <strong>Fale conosco</strong>
        <p>
          <FiMapPin size={20} />
          {' '}
          <span>R. Sete de Setembro, 138 - Centro</span>
        </p>
        <p>
          <FiPhone size={20} />
          {' '}
          <span>(35) 3651-1437</span>
        </p>
        <p>
          <FaWhatsapp size={20} />
          {' '}
          <span>(35) 984396262</span>
        </p>
        <p>
          <FiMail size={20} />
          {' '}
          <span>fariaelima@hotmail.com</span>
        </p>
      </div>
      <div className={styles.subFooter}>
        <span>Todos os direitos reservados a Faria e lima.</span>
        <span>Desenvolvido por: BTR Soluções Inovadoras</span>
      </div>
    </footer>
  );
}
