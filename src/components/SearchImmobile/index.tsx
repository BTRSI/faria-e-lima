import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import handleErrors from '@/utils/handleErrors';
import { useRouter } from 'next/router';
import React, {
  FormEvent, useCallback, useEffect, useState,
} from 'react';
import { toast } from 'react-toastify';
import { ufList } from '@/constants/UF';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { InputSearch } from '../InputSearch';
import styles from './styles.module.scss';

interface ICategory {
    id_categoria: string;
    titulo: string;
}

interface ICostumer {
    id_proprietario: string;
    nome: string;
}

interface IModality {
    id_modalidade: string;
    titulo: string;
}

interface ICity {
    id: string;
    nome: string;
}

interface IFilter {
    id_cidade_fk?: string;
    id_categoria_fk?: string;
    rua?: string;
    bairro?: string;
    garagem?: string;
    id_proprietario_fk?: string;
    id_modalidade_fk?: string;
}

interface SearchImmobileProps {
    onHandleSearchToImmobile: (filteredImmobile: any[]) => void;
    categories?: ICategory[] | null;
}

export function SearchImmobile({ onHandleSearchToImmobile, categories }: SearchImmobileProps) {
  const { setLoading } = useLoading();
  const [filter, setFilter] = useState<IFilter>({} as IFilter);
  const { query } = useRouter();
  const [ufSelected, setUfSelected] = useState('');
  const [cities, setCities] = useState<ICity[]>([]);
  const [categoryList, setCategoryList] = useState<ICategory[]>([]);
  const [costumerList, setCosutmerList] = useState<ICostumer[]>([]);
  const [modalityList, setModalityList] = useState<IModality[]>([]);

  useEffect(() => {
    function getInformation() {
      api.post('/categoria/pesquisa', { categoria: {} }).then((response) => {
        setCategoryList(response.data.result);
      });

      api.post('/proprietario/pesquisa', { proprietario: {} }).then((response) => {
        setCosutmerList(response.data.result);
      });

      api.post('/modalidade/pesquisa', { modalidade: {} }).then((response) => {
        setModalityList(response.data.result);
      });
    }

    if (categories && query.type) {
      setCategoryList(categories);
    } else {
      getInformation();
    }
  }, [categories, query]);

  useEffect(() => {
    if (ufSelected !== '') {
      setLoading(true);

      api.post('/cidade/pesquisa', {
        cidade: {
          uf: ufSelected,
        },
      }).then((response) => {
        setLoading(false);
        setCities(response.data);
      }).catch(() => {
        setLoading(false);
        setCities([]);
      });
    }
  }, [ufSelected, setLoading]);

  const handleGetImmobile = useCallback(async (filterImmobile?: any) => {
    setLoading(true);
    try {
      const response = await api.post('/imovel/pesquisa', {
        imovel: {
          status: 'ATIVO',
          ...filterImmobile,
        },
      });

      setLoading(false);
      onHandleSearchToImmobile(response.data.result.map((immobileMapped) => ({
        ...immobileMapped,
        valorFormatado: formatCurrencyToBrazil(Number(immobileMapped.valor)),
        banner: immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM')
          ? immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM').file
          : immobileMapped.imagens[0]?.file ?? '',
      })));
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else {
        toast.error('Erro ao fazer busca!');
      }
    }
  }, [onHandleSearchToImmobile, setLoading]);

  const handleFilter = useCallback(async (event?: FormEvent) => {
    if (event) {
      event.preventDefault();
    }

    const { type } = query;

    if (type === 'venda') {
      filter.id_modalidade_fk = '1';
      setFilter(filter);
    } else if (type === 'aluguel') {
      filter.id_modalidade_fk = '2';
      setFilter(filter);
    }

    if (filter.bairro === '') {
      delete filter.bairro;
    }

    if (filter.garagem === '') {
      delete filter.garagem;
    }

    if (filter.id_categoria_fk === '') {
      delete filter.id_categoria_fk;
    }

    if (filter.id_cidade_fk === '') {
      delete filter.id_cidade_fk;
    }

    if (filter.rua === '') {
      delete filter.rua;
    }

    if (filter.id_categoria_fk === '') {
      delete filter.id_categoria_fk;
    }

    if (filter.id_modalidade_fk === '') {
      delete filter.id_modalidade_fk;
    }

    if (filter.id_proprietario_fk === '') {
      delete filter.id_proprietario_fk;
    }

    handleGetImmobile(filter);
  }, [filter, handleGetImmobile, query]);

  const handleRemoveFilter = useCallback(async () => {
    setFilter({
      bairro: '',
      garagem: '',
      id_categoria_fk: '',
      id_cidade_fk: '',
      rua: '',
      id_proprietario_fk: '',
      id_modalidade_fk: '',
    });
    setUfSelected('');
    if (query.type) {
      if (query.type === 'venda') {
        filter.id_modalidade_fk = '1';
        setFilter(filter);
      } else if (query.type === 'aluguel') {
        filter.id_modalidade_fk = '2';
        setFilter(filter);
      }
      handleGetImmobile({
        id_modalidade_fk: filter.id_modalidade_fk,
      });
    } else {
      handleGetImmobile();
    }
  }, [filter, handleGetImmobile, query.type]);

  return (
    <section className={styles.searchContainer}>

      <form className={styles.formContainer} onSubmit={handleFilter}>
        <h1>Busque pelo imóvel</h1>
        <h1>{query.type === 'venda' ? 'Venda' : query.type === 'aluguel' ? 'Aluguel' : '' }</h1>
        <div className={styles.formContent}>
          <div>
            <label htmlFor="uf">UF</label>
            <select
              id="uf"
              value={ufSelected}
              onChange={(e) => { setUfSelected(e.target.value); }}
            >
              <option value="">Selecione</option>
              {ufList.map((ufMapped) => (
                <option key={ufMapped.value} value={ufMapped.value}>{ufMapped.label}</option>
              ))}
            </select>
            <label htmlFor="cidade">Cidade</label>
            <select
              id="cidade"
              value={filter.id_cidade_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_cidade_fk: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              {cities.map((cityMapped) => (
                <option key={cityMapped.id} value={cityMapped.id}>{cityMapped.nome}</option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="tipo">Endereço:</label>
            <InputSearch
              value={filter.rua}
              placeholder="Todos"
              onChange={(e) => {
                setFilter({
                  ...filter,
                  rua: e.target.value,
                });
              }}
            />
            <label htmlFor="bairro">Bairro:</label>
            <InputSearch
              value={filter.bairro}
              placeholder="Todos"
              onChange={(e) => {
                setFilter({
                  ...filter,
                  bairro: e.target.value,
                });
              }}
            />
          </div>
          <div>
            <label htmlFor="garagem">Garagem</label>
            <select
              id="garagem"
              value={filter.garagem}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  garagem: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              <option value="SIM">SIM</option>
              <option value="NAO">NÃO</option>
            </select>
            <label htmlFor="categoria">Categoria</label>
            <select
              id="categoria"
              value={filter.id_categoria_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_categoria_fk: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              {categoryList.map((categoryMapped) => (
                <option
                  key={categoryMapped.id_categoria}
                  value={categoryMapped.id_categoria}
                >
                  {categoryMapped.titulo}

                </option>
              ))}
            </select>
          </div>
        </div>
        {!query.type && (
        <div className={styles.filterFlexContainer}>
          <div>
            <label htmlFor="cliente">Clientes</label>
            <select
              id="cliente"
              value={filter.id_proprietario_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_proprietario_fk: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              {costumerList.map((categoryMapped) => (
                <option
                  key={categoryMapped.id_proprietario}
                  value={categoryMapped.id_proprietario}
                >
                  {categoryMapped.nome}

                </option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="modalidade">Tipo</label>
            <select
              id="modalidade"
              value={filter.id_modalidade_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_modalidade_fk: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              {modalityList.map((modalityMapped) => (
                <option
                  key={modalityMapped.id_modalidade}
                  value={modalityMapped.id_modalidade}
                >
                  {modalityMapped.titulo}

                </option>
              ))}
            </select>
          </div>

        </div>
        )}
        <div className={styles.containerButton}>
          <button type="submit">Buscar</button>
          <button
            type="button"
            onClick={handleRemoveFilter}
          >
            Zerar filtro

          </button>
        </div>
      </form>
    </section>
  );
}
