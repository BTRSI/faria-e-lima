import { useSelect } from '@/hooks/useSelect';
import { useSidebar } from '@/hooks/useSidebar';
import { useCallback } from 'react';
import styles from '../styles.module.scss';

export function HeaderAdminPage(): JSX.Element {
  const { isSelect, setIsSelect } = useSelect();
  const { setOpen } = useSidebar();

  const handleOptionSelected = useCallback((option: 'imovel' | 'usuario' | 'cliente') => {
    setIsSelect(option);
    setOpen(false);
  }, [setIsSelect, setOpen]);

  return (
    <>
      <nav className={styles.nav}>
        <button
          type="button"
          onClick={() => { handleOptionSelected('cliente'); }}
          className={isSelect === 'cliente' ? `${styles.active}` : ''}
        >
          <a>Cliente</a>
        </button>
      </nav>
      <nav className={styles.nav}>
        <button
          className={isSelect === 'imovel' ? `${styles.active}` : ''}
          type="button"
          onClick={() => { handleOptionSelected('imovel'); }}
        >
          <a>Imóvel</a>
        </button>
      </nav>
      <nav className={styles.nav}>
        <button
          onClick={() => { handleOptionSelected('usuario'); }}
          type="button"
          className={isSelect === 'usuario' ? `${styles.active}` : ''}
        >
          <a>Usuario</a>
        </button>
      </nav>
    </>
  );
}
