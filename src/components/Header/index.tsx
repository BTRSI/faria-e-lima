import Hamburger from 'hamburger-react';
import { useSidebar } from '@/hooks/useSidebar';
import React, { useCallback } from 'react';
import { FaPhoneAlt } from 'react-icons/fa';
import { FiLogOut } from 'react-icons/fi';
import { useAuth } from '@/hooks/useAuth';
import styles from './styles.module.scss';
import { HeaderLandingPage } from './HeaderLandingPage';
import { HeaderAdminPage } from './HeaderAdminPage';

interface HeaderProps {
    type: 'page' | 'admin';
}

export function Header({ type }: HeaderProps): JSX.Element {
  const { setOpen, open } = useSidebar();
  const { signOut } = useAuth();

  const handleLogout = useCallback(() => {
    signOut();
  }, [signOut]);

  return (
    <header className={styles.headerContainer}>
      <div className={styles.headerContent}>
        <div className={styles.hamburguer}>
          <Hamburger
            toggle={() => { setOpen(!open); }}
            toggled={open}
          />
        </div>
        <img src="/images/logo.svg" alt="logo" />
        {type === 'page'
                    && (
                    <>
                      <div className={styles.menuLandingPage}>
                        <HeaderLandingPage />
                      </div>
                      <div className={styles.phone}>
                        <FaPhoneAlt size={16} />
                        <a href="#">(35) 3651-1437</a>
                      </div>
                    </>
                    )}
        {type === 'admin'
                    && (
                    <>
                      <div className={styles.menuAdmin}>
                        <HeaderAdminPage />
                      </div>
                      {!open
                            && (
                            <button
                              type="button"
                              className={styles.buttonLogout}
                              onClick={handleLogout}
                            >
                              <FiLogOut color="#57727d" size={20} />
                            </button>
                            )}
                    </>
                    )}
      </div>
    </header>
  );
}
