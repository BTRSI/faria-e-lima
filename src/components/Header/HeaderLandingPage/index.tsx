import { ActiveLink } from '@/components/ActiveLink';
import { useSidebar } from '@/hooks/useSidebar';

import styles from '../styles.module.scss';

export function HeaderLandingPage(): JSX.Element {
  const { setOpen } = useSidebar();

  return (
    <>
      <nav onClick={() => { setOpen(false); }} className={styles.nav}>
        <ActiveLink activeClassName={styles.active} href="/">
          <a>Home</a>
        </ActiveLink>
      </nav>
      <nav onClick={() => { setOpen(false); }} className={styles.nav}>
        <ActiveLink activeClassName={styles.active} href="/imoveis/venda">
          <a>Venda</a>
        </ActiveLink>
      </nav>
      <nav onClick={() => { setOpen(false); }} className={styles.nav}>
        <ActiveLink activeClassName={styles.active} href="/imoveis/aluguel">
          <a>Aluguel</a>
        </ActiveLink>
      </nav>
      <nav onClick={() => { setOpen(false); }} className={styles.nav}>
        <ActiveLink activeClassName={styles.active} href="/contato">
          <a>Contato</a>
        </ActiveLink>
      </nav>
    </>
  );
}
