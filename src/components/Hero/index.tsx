import styles from './styles.module.scss';

export function Hero(): JSX.Element {
  return (
    <main className={styles.contentContainer}>
      <section className={styles.hero}>
        <span>😊  Olá, bem vindo</span>
        <h1>Encontre imóveis a venda com facilidade</h1>
        <p>
          As melhores opções de imóvel para você.
          <br />
        </p>
      </section>
      <img src="/images/logo.svg" alt="logo" />
    </main>
  );
}
