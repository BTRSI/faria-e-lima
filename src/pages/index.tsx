import { Hero } from '@/components/Hero';
import Footer from '@/components/Footer';
import { SomeProperties } from '@/components/SomeProperties';
import { Header } from '@/components/Header';
import SEO from '@/components/SEO';
import { GetStaticProps } from 'next';
import { api } from '@/services/axios';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';

interface HomeProps {
  immobile: unknown[];
}

export default function Home({ immobile }: HomeProps): JSX.Element {
  return (
    <>
      <SEO title="Faria & Lima" />
      <Header type="page" />
      <Hero />
      <SomeProperties immobile={immobile} />
      <Footer />
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const response = await api.post('/imovel/pesquisa', {
    imovel: {
      status: 'ATIVO',
    },
    rows: 6,
  });

  const immobile = response.data.result.map((immobileMapped) => ({
    ...immobileMapped,
    valorFormatado: formatCurrencyToBrazil(Number(immobileMapped.valor)),
    banner: immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM')
      ? immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM').file
      : immobileMapped.imagens[0]?.file ?? '',
  }));

  return {
    props: {
      immobile,
    },
    revalidate: 30, // 30 segundos
  };
};
