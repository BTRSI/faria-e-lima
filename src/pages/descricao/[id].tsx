import Footer from '@/components/Footer';
import { Header } from '@/components/Header';
import SEO from '@/components/SEO';
import { api } from '@/services/axios';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { GetStaticProps } from 'next';
import ModalImage from 'react-modal-image';
import styles from './styles.module.scss';

interface IImmobile {
    id_imovel: string;
    titulo: string;
    descricao: string;
    valor: string;
    comodos: string;
    dimensao: string;
    garagem: string;
    coordenadas: string;
    link_maps: string;
    rua: string;
    numero: string;
    bairro: string;
    id_proprietario_fk: string;
    id_modalidade_fk: string;
    id_categoria_fk: string;
    id_cidade_fk: string;
    valorFormatado: string;
    categoria: string;
    tipo: string;
    cidade: string;
    uf: string;
    imagens: {
      id_imovel_imagem: string;
      file: string;
      destaque: 'SIM' | 'NAO'
    }[]
}

interface DescricaoProps {
    immobile: IImmobile;
}

export default function Descricao({ immobile }: DescricaoProps): JSX.Element {
  return (
    <>
      <SEO title="Faria & Lima | Descrição" />
      <Header type="page" />
      <main className={styles.container}>
        <div className={styles.content}>
          <h1>Descrição</h1>
          <p>
            Titulo:
            {' '}
            <span>{immobile.titulo}</span>
          </p>
          <p>
            Tipo:
            {' '}
            <span>{immobile.tipo}</span>
          </p>
          <p>
            Valor:
            {' '}
            <span>{immobile.valorFormatado}</span>
          </p>
          <p>
            Comodos:
            {' '}
            <span>{immobile.comodos}</span>
          </p>
          <p>
            Dimensao:
            {' '}
            <span>
              {immobile.dimensao}
              m2
            </span>
          </p>
          <p>
            Garagem:
            {' '}
            <span>{immobile.dimensao}</span>
          </p>
          <p>
            Coordenadas:
            {' '}
            <span>{immobile.coordenadas}</span>
          </p>
          <p>
            Link:
            {' '}
            <a href={immobile.link_maps} target="_blank" rel="noreferrer">Acessar mapa</a>
          </p>
          <p>
            Estado:
            {' '}
            <span>{immobile.uf}</span>
          </p>
          <p>
            Cidade:
            {' '}
            <span>{immobile.cidade}</span>
          </p>
          <p>
            Endereço:
            {' '}
            <span>{immobile.rua}</span>
          </p>
          <p>
            Número:
            {' '}
            <span>{immobile.numero}</span>
          </p>
          <p>
            Bairro:
            {' '}
            <span>{immobile.bairro}</span>
          </p>
          <p>
            Categoria:
            {' '}
            <span>{immobile.categoria}</span>
          </p>
          <p>
            Descrição:
            <span>{immobile.descricao}</span>
          </p>

          <h1>Imagens</h1>
          <div className={styles.imgContainer}>
            {immobile.imagens.map((imageMapped) => (
              <ModalImage
                key={imageMapped.id_imovel_imagem}
                small={`http://mobi.tieng.com.br/imagens/${immobile.id_imovel}/${imageMapped.file}`}
                large={`http://mobi.tieng.com.br/imagens/${immobile.id_imovel}/${imageMapped.file}`}
                alt="Imagens"
                className={styles.smalImage}
              />
            ))}
          </div>

        </div>
      </main>
      <Footer />
    </>
  );
}

export const getStaticPaths = () => ({
  paths: [],
  fallback: 'blocking',
});

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { id } = params;

  const response = await api.get(`/imovel/${id}`);
  let immobile = response.data.result[0];
  const modalityResponse = await api.get(`/modalidade/${immobile.id_modalidade_fk}`);
  const categoryResponse = await api.get(`/categoria/${immobile.id_categoria_fk}`);
  const cityResponse = await api.get(`/cidade/${immobile.id_cidade_fk}`);

  immobile = {
    ...immobile,
    valorFormatado: formatCurrencyToBrazil(Number(immobile.valor)),
    categoria: categoryResponse.data.titulo,
    tipo: modalityResponse.data.titulo,
    cidade: cityResponse.data.nome,
    uf: cityResponse.data.uf,
  };

  return {
    props: {
      immobile,
    },
    revalidate: 30,
  };
};
