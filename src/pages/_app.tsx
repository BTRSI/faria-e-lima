import { AppProps } from 'next/app';
import AppProvider from '@/hooks';
import { ToastContainer } from 'react-toastify';
import NProgress from 'nprogress';
import Router from 'next/router';
import '@/styles/global.scss';
import 'react-toastify/dist/ReactToastify.css';
import 'nprogress/nprogress.css';

Router.events.on('routeChangeStart', () => {
  NProgress.start();
});

Router.events.on('routeChangeComplete', () => {
  NProgress.done();
});

Router.events.on('routeChangeError', () => {
  NProgress.done();
});

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <AppProvider>
      <Component {...pageProps} />
      <ToastContainer autoClose={3000} />
    </AppProvider>
  );
}

export default MyApp;
