import { FiPhone, FiMapPin, FiMail } from 'react-icons/fi';
import { FaWhatsapp } from 'react-icons/fa';
import { Header } from '@/components/Header';
import SEO from '@/components/SEO';
import styles from './styles.module.scss';

export default function Contact(): JSX.Element {
  return (
    <>
      <SEO title="Faria & Lima" />
      <Header type="page" />
      <main className={styles.container}>
        <div className={styles.Card}>
          <section>
            <h1>Como nos encontrar</h1>
            <p>
              <FiMapPin size={20} />
              {' '}
              <span>R. Sete de Setembro, 138 - Centro</span>
            </p>
            <p>
              <FiPhone size={20} />
              {' '}
              <span>(35) 3651-1437</span>
            </p>
            <p>
              <FaWhatsapp size={20} />
              {' '}
              <span>(35) 984396262</span>
            </p>
            <p>
              <FiMail size={20} />
              {' '}
              <span>fariaelima@hotmail.com</span>
            </p>
          </section>
          <section>
            <h1>Horário de abertura</h1>
            <p className={styles.hour}>
              Segunda a Sexta:
              {' '}
              <span>09:00 - 17:00</span>
            </p>
            <p className={styles.hour}>
              Sabado:
              {' '}
              <span>10:00 - 14:00</span>
            </p>
            <p className={styles.hour}>
              Domingo:
              {' '}
              <span>Fechado</span>
            </p>
          </section>
        </div>
        <img src="/images/logo.svg" alt="logo" />
      </main>
    </>
  );
}
