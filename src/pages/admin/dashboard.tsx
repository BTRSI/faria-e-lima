import { Immobile } from '@/components/Dashboard/Immobile';
import { Costumer } from '@/components/Dashboard/Costumer';
import { User } from '@/components/Dashboard/User';
import { Header } from '@/components/Header';
import SEO from '@/components/SEO';
import { useSelect } from '@/hooks/useSelect';
import { GetServerSideProps } from 'next';
import styles from './dashboard.module.scss';

export default function Dashboard(): JSX.Element {
  const { isSelect } = useSelect();

  return (
    <>
      <SEO shouldExcludeTitleSuffix title="Dashboard | Faria & Lima" />
      <Header type="admin" />
      <main className={styles.container}>
        <div className={styles.content}>
          {isSelect === 'cliente' && <Costumer />}
          {isSelect === 'imovel' && <Immobile />}
          {isSelect === 'usuario' && <User />}
        </div>
      </main>
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const { token } = req.cookies;

  if (!token) {
    return {
      redirect: {
        destination: '/admin/login',
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};
