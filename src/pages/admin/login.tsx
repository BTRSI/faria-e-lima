import SEO from '@/components/SEO';
import { useAuth } from '@/hooks/useAuth';
import { useLoading } from '@/hooks/useLoading';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import { FormEvent, useCallback, useState } from 'react';
import styles from './login.module.scss';

export default function Admin(): JSX.Element {
  const router = useRouter();
  const { setLoading } = useLoading();
  const { signIn } = useAuth();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSignIn = useCallback(async (event: FormEvent) => {
    event.preventDefault();
    setLoading(true);
    try {
      await signIn({
        l: email,
        p: password,
      });

      setLoading(false);
      router.push('/admin/dashboard');
    } catch (err) {
      setLoading(false);
    }
  }, [email, password, router, signIn, setLoading]);

  return (
    <>
      <SEO title="Login | Faria e Lima" shouldExcludeTitleSuffix />
      <main className={styles.container}>
        <form onSubmit={handleSignIn} className={styles.loginContainer}>
          <label htmlFor="email">E-mail</label>
          <input
            placeholder="Digite seu E-mail"
            id="email"
            type="text"
            value={email}
            onChange={(e) => { setEmail(e.target.value); }}
          />

          <label htmlFor="senha">Senha</label>
          <input
            placeholder="Digite sua senha"
            id="senha"
            type="password"
            value={password}
            onChange={(e) => { setPassword(e.target.value); }}
          />

          <button type="submit">Entrar</button>
        </form>
        <section className={styles.backgroundContainer}>
          <img src="/images/logo.svg" alt="logo" />
        </section>
      </main>
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const { token } = req.cookies;

  if (token) {
    return {
      redirect: {
        destination: '/admin/dashboard',
        permanent: false,
      },
    };
  }
  return {
    props: {},
  };
};
